BeecasH integration/staging tree
================================

http://www.beecash.co

Copyright (c) 2009-2014 Bitcoin Developers
Copyright (c) 2011-2014 Litecoin Developers
Copyright (c) 2015-2018 Beecash Developers



What is Beecash?
----------------
BeecasH is a lite version of Litecoin using scrypt as a proof-of-work algorithm.

Ticker: BEH

Denarius [BEH] is an anonymous, untraceable, energy efficient, Proof-of-Work (Scrypt Algorithm) and Proof-of-Stake cryptocurrency.

26,000,000 BEH will be created in about 4 years during the PoW phase. Denarius has a 50% Premine of 26,000,000 BEH for bounties, distribution, and marketing.

For more information, as well as an immediately useable, binary version of
the BeecasH client sofware, see http://www.beecash.co.

Specifications
-------
Total number of coins: 26,000,000 BEH
Ideal block time: 30 seconds
Confirmations: 10 blocks
Maturity: 30 blocks (15 minutes)
Min stake age: 8 hours


Technology
-------

Stealth addresses
Encrypted Messaging
Multi-Signature Addresses & TXs
Fast 30 Second Block Times
Scrypt PoW Algorithm 
Full decentralization

Download Wallet HOW TO SET UP BEECASH COIN WALLET ON WINDOWS OPERATING SYSTEM?
-------------------


A “BeeCash wallet” is basically the BeeCash Coin Account, which allows you to receive BeeCash Coin, store them, and then send them to others.

Click here : http://www.beecash.co/wallet/beecash-wallet.zip

Unzip the wallets files.
You will get beecash.exe file, Install the wallet software by double click on beecash.exe file.
You can now send and receive BeeCash Coin directly from BeeCash Desktop Wallet and also use this wallet to stake BeeCash Coin.

HOW TO SET UP BEECASH COIN WALLET ON LINUX OPERATING SYSTEM?

Click here : http://www.beecash.co/wallet/beecash-linux-qt.zip

Open linux terminal and go to destination path of downloaded directory.
Unzip the wallets files using command unzip wallet_file.zip -d destination_folder.
execute the wallet file using command ./beecash-linux-qt.

Unzip the wallets files.
You will get beecash-qt.dmg file, Install the wallet software by double click on beecash-qt.dmg file.
You can now send and receive BeeCash Coin directly from BeeCash Desktop Wallet and also use this wallet to stake BeeCash Coin.

